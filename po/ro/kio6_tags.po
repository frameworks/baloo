# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the baloo package.
# Sergiu Bivol <sergiu@cip.md>, 2014, 2022, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-16 02:14+0000\n"
"PO-Revision-Date: 2024-01-07 12:45+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: kio_tags.cpp:110 kio_tags.cpp:166
#, kde-format
msgid "File %1 already has tag %2"
msgstr "Fișierul %1 are deja marcajul %2"

#: kio_tags.cpp:298
#, kde-format
msgctxt "This is a noun"
msgid "Tag"
msgstr "Marcaj"

#: kio_tags.cpp:304
#, kde-format
msgctxt "This is a noun"
msgid "Tag Fragment"
msgstr "Fragment de marcaj"

#: kio_tags.cpp:316 kio_tags.cpp:317
#, kde-format
msgid "All Tags"
msgstr "Toate marcajele"

#, fuzzy
#~| msgid "All Tags"
#~ msgid "Tags"
#~ msgstr "Toate marcajele"
